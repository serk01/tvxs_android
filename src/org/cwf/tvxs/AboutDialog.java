/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "A simple about dialog"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
*/

package org.cwf.tvxs;

import android.app.AlertDialog.Builder;
import android.app.AlertDialog;
import android.graphics.Color;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.widget.TextView;
import android.content.Context;
import android.view.Gravity;
import android.text.SpannableString;
import android.text.util.Linkify;
import java.lang.StringBuffer;
import android.content.res.Resources;
import android.content.pm.PackageManager.NameNotFoundException;
    
public class AboutDialog {
    public static AlertDialog create(Context c, Boolean isAbout) throws NameNotFoundException {
	Resources res = c.getResources();
	
	//try to load a package matching the name of our own package
	PackageInfo pInfo = c.getPackageManager().getPackageInfo(c.getPackageName(), PackageManager.GET_META_DATA);
	String versionInfo = pInfo.versionName;
	String title = "";

	//crate the Textview that will be the view to hold our text
	final TextView message = new TextView(c);
	
	//crate a SpannableString so we can Linify the text
	final SpannableString s;

	//use a StringBuffer to create the final text
	StringBuffer msg = new StringBuffer();

	if (isAbout){
	    //create About dialog
	    title = String.format("About %s v%s",c.getString(R.string.app_name),versionInfo);
	    msg.append(res.getString(R.string.about)+"\n\n");
	    msg.append(res.getString(R.string.copyleft));
	    s = new SpannableString(msg.toString());
	} else {
	    //create intro dialog
	    msg.append(res.getString(R.string.intro));
	    s = new SpannableString(msg.toString());
	}
	message.setText(s);
	message.setTextColor(Color.WHITE);
	message.setPadding(5 , 5 , 5 , 5);
	message.setGravity(Gravity.CENTER);
	Linkify.addLinks(message,Linkify.WEB_URLS);

	if (isAbout)
	    return new AlertDialog.Builder(c).setTitle(title).setCancelable(true).setView(message).create();
	else
	    return new AlertDialog.Builder(c).setCancelable(true).setView(message).create();
	    
    }
}


    

