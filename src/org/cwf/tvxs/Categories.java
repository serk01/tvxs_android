/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "A list of all categories. Accessed through the Menu"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
*/

package org.cwf.tvxs;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.view.KeyEvent;
import android.widget.BaseAdapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import java.lang.StringBuilder;
import java.util.StringTokenizer;

public class Categories extends ListActivity implements AdapterView.OnItemClickListener {
  
    private static final String TAG = "Categories";
    private ArrayList<String> mActiveCategories = new ArrayList<String>();
    private ListView mListV;
    private Boolean mSelectionChanged = false;
    private Boolean abortBack = false;
    private SharedPrefsController sharedPrefsCntr;
  
    @Override
	public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	Log.d(TAG, "Categories loaded");
	
	sharedPrefsCntr = new SharedPrefsController(this);
	mListV = this.getListView();
	mListV.setAdapter(new CategorySelectionListAdapter());
	mListV.setOnItemClickListener(this);
	
    }

    @Override
	protected void onResume(){
	super.onResume();
	getSharedPrefs();
    
    }

    @Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
    
	if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	    Log.d(TAG, "back button pressed");

	    if (mSelectionChanged == true){
	    	abortBack = true;
	    	new AlertDialog.Builder(this)
		    .setMessage(R.string.save_question)
		    .setCancelable(true)
		    .setPositiveButton(R.string.save,new DialogInterface.OnClickListener(){
			    public void onClick(DialogInterface dialog,int b){
			    	savePrefs(true);
			    	return;
			    }
			})
		    .setNegativeButton(R.string.dontsave, new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int whichButton){
			    	closeActivity(); //close Activity from an outside method. This doesn't seem right to me but will go with it for now
			    }
			})
		    .show();
	    }
	    else
	    	savePrefs(false);
      
	    if (abortBack == false)
		return super.onKeyDown(keyCode, event);
	    else
		return false;

	} else
	    return super.onKeyDown(keyCode, event);
    }

    private void closeActivity(){
	this.finish();
    }

    private void savePrefs(Boolean changesMade){
	sharedPrefsCntr.saveCategoriesToPrefs(mActiveCategories, changesMade);
	closeActivity();
    }

    private void getSharedPrefs(){

	mActiveCategories = sharedPrefsCntr.retrieveSavedCategoriesFromPrefs();	

    }
    
    public void onItemClick(AdapterView<?>  parent, View  view, int position, long id){

	String categName = getListModel(position);
	// Toast.makeText(getApplicationContext(),categName, Toast.LENGTH_SHORT).show();
	
	if (mActiveCategories.contains(categName))
	    mActiveCategories.remove(categName);
	else {
	    mActiveCategories.add(categName);
	}
	
	// Log.d(TAG,"Active Categories:"+mActiveCategories);
	mSelectionChanged = true;
	BaseAdapter b = (BaseAdapter)mListV.getAdapter();
	b.notifyDataSetChanged(); //reloading the ListView every time an item changes is not efficient but with this size of a list user doesn't notice

    }

    private String getListModel(int position) {

	return((String)(this.getListView().getAdapter()).getItem(position));
    }
  
    private class CategorySelectionListAdapter extends ArrayAdapter<String> {

	CategorySelectionListAdapter() {
	    super(getApplicationContext(), R.layout.category_select__row, FeedURLS.CATEGORIES_NAMES);
	}
    
	public View getView(int position, View convertView, ViewGroup parent){

	    View row = convertView;

	    CatSelectionWrapper wrapper = null;
      
	    if (row == null){
		LayoutInflater inflater = getLayoutInflater();
		row = inflater.inflate(R.layout.category_select__row, parent, false);
		wrapper = new CatSelectionWrapper(row);
		row.setTag(wrapper);
	    } else {
		wrapper = (CatSelectionWrapper)row.getTag();
	    }
      
	    String title = getListModel(position);

	    //if no active categories, this is the first launch, populate with defaults
	    if (mActiveCategories.isEmpty()){
		mActiveCategories.add(FeedURLS.CATEGORIES_NAMES[0]);
		mActiveCategories.add(FeedURLS.CATEGORIES_NAMES[1]);
		mActiveCategories.add(FeedURLS.CATEGORIES_NAMES[2]);
	    }

	    wrapper.getTitleTextView().setText(title);
	    wrapper.getStatusImage().setImageResource(R.drawable.tick);
      
	    if (mActiveCategories.contains(title)){
		wrapper.getStatusImage().setVisibility(View.VISIBLE);
		wrapper.setStatus(true);
	    }	else {
		wrapper.getStatusImage().setVisibility(View.INVISIBLE);
		wrapper.setStatus(false);
	    }

	    return row;
	}
 
	public class CatSelectionWrapper{
	    View base;
	    TextView title;
	    ImageView img;
	    Boolean status;

	    CatSelectionWrapper(View base){
		this.base = base;
	    }
      
	    TextView getTitleTextView(){
		if (title == null)
		    title = (TextView)base.findViewById(R.id.catTitle);

		return title;
	    }

	    ImageView getStatusImage(){
		if (img == null)
		    img = (ImageView)base.findViewById(R.id.statusimg);

		return img;
	    }

	    Boolean getStatus(){
		return status;
	    }

	    void setStatus(Boolean st){
		status = st;
	    }

	}
    }
} 