/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "Constants for the app"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
*/

package org.cwf.tvxs;


public final class Constants {

    public static final int TITLE_SCROLLER = 0x04;
    public static final int TITLE_SCROLLER_HEIGHT = 40;
    public static final int FAIL = 0x010;
    public static final int MINUSONEITEM = 0x011;
    public static final String CATEGORIES_TAG = "CATEGORIES";
    public static final String NEW_CATEGORIES_TAG = "NEW_CATEGORIES";
    public static final String HAS_CHANGED_FLAG = "CHANGED";
    public static final String IS_FIRST_LAUNCH = "IS FIRST LAUNCH";

    //Database fields
    public static final String KEY_ROWID = "_id";
	public static final String KEY_TITLE = "title";	
	public static final String KEY_TEASER = "teaser";
	public static final String KEY_FULL_ARTICLE = "fullarticle";
	public static final String KEY_PUBDATE = "pubdate";
	public static final String KEY_ARITCLE_URL = "articleurl";
	public static final String KEY_IMAGE = "image";	
	public static final String KEY_IMGURL = "imageurl";
    //preferences keys
    public static final String FullArticleAutoLoad_key = "autoload_article";
}