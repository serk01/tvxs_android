package org.cwf.tvxs;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DateSorter;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class FavouritesActivity extends ListActivity implements AdapterView.OnItemClickListener
{
	private static final String TAG = "FavouritesActivity";
	private ListView mainListView = null;
	//	CustomSqlCursorAdapter adapter = null;
	private TvxsDbAdapter dbAdapter = null;
	private FavouritesCursorAdapter dataSource = null;
	private Cursor dataCursor;
	
	private ListView listView = null;
	/** called when the activity is created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favouriteslist);

		if (this.dbAdapter == null)
			this.dbAdapter = new TvxsDbAdapter(this);

		dbAdapter.open();


		// the desired columns to be bound
		String[] columns = new String[] {Constants.KEY_TITLE,Constants.KEY_PUBDATE};

		// the XML defined views which the data will be bound to
		int[] toFields = new int[] { R.id.rowTitle, R.id.rowDate };

		dataCursor = dbAdapter.fetchAllFavourites();		
		dataSource = new FavouritesCursorAdapter(this, R.layout.simple_row, 
				dataCursor, 
				columns, 
				toFields);

		this.setListAdapter(dataSource);
		this.getListView().setOnItemClickListener(this);
	}


	public void onItemClick(AdapterView<?>  parent, View  view, int position, long id){
		Intent i = new Intent(this, TvxsArticleDetailActivity.class);

		i.putExtra("title", dataCursor.getString(dataCursor.getColumnIndex(Constants.KEY_TITLE)));
		i.putExtra("teaser", dataCursor.getString(dataCursor.getColumnIndex(Constants.KEY_TEASER)));
		i.putExtra("url", dataCursor.getString(dataCursor.getColumnIndex(Constants.KEY_ARITCLE_URL)));
		i.putExtra("pubdate",dataCursor.getString(dataCursor.getColumnIndex(Constants.KEY_PUBDATE)));
		i.putExtra("imgurl",dataCursor.getString(dataCursor.getColumnIndex(Constants.KEY_IMGURL)));
		i.putExtra("imgblob",dataCursor.getBlob(dataCursor.getColumnIndex(Constants.KEY_IMAGE)));
		startActivity(i);
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		dataCursor.close();
		dbAdapter.close();
	}
	
//	@Override
	protected void onResume()
	{
		super.onResume();
		dataCursor.requery();
	}
}