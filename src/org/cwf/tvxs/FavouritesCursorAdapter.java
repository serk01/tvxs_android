package org.cwf.tvxs;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;


public class FavouritesCursorAdapter extends SimpleCursorAdapter
{
	final static String TAG = "FavouritesCursorAdapter";
	private Context context;
	private int layout;
	private ImageLoader_lazy imageLoader;
	private Activity activity = null;

	//constructor
	public FavouritesCursorAdapter(Context context, int layout, Cursor c,
			String[] from, int[] toFields)
	{
		super(context, layout, c, from, toFields);
		this.context = context;
		this.layout = layout;
		imageLoader = new ImageLoader_lazy(context);


	}


	//	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		//		// TODO Auto-generated method stub
		final LayoutInflater inflater = LayoutInflater.from(context);
		View row = inflater.inflate(layout, parent,false);

		return row;
	}

	@Override
	public void bindView(View v, Context context, Cursor c) {

		NewsRowWrapper wrapper = null;
		wrapper = new NewsRowWrapper(v);

		String title = c.getString(c.getColumnIndex("title"));
		String pubDate = c.getString(c.getColumnIndex("pubdate"));
		String imgurl = c.getString(c.getColumnIndex("imageurl"));

		wrapper.getTitleTextView().setText(title);
		wrapper.getDateTextView().setText(pubDate);

		byte[] image = c.getBlob(c.getColumnIndex("image"));
		ImageView imageView = wrapper.getImageView();

		if (image != null)
			imageView.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));
		else 
		{
			if (imgurl!=null)
			{
				imageView.setTag(imgurl);
				imageLoader.DisplayImage(imgurl, (Activity)context, imageView);
			}
			else {
				Log.d(TAG,"empty image");
				imageView.setImageResource(R.drawable.imageplaceholder);				
			}
		}
	}
}

