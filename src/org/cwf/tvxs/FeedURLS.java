/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "All the urls and their respective titles that are used in the app"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
 */

package org.cwf.tvxs;
import java.util.HashMap;

public final class FeedURLS {

	public static final String TITLE_NEWS = "Νέα";
	public static final String TITLE_GREECE = "Ελλάδα";
	public static final String TITLE_WORLD = "Kόσμος";
	public static final String TITLE_LOCAL = "Τοπικά";
	public static final String TITLE_SCITECH = "Sci-tech";
	public static final String TITLE_SPORT = "Αθλητικά";
	public static final String TITLE_INTERNET = "Ιντερνετ-ΜΜΕ";
	public static final String TITLE_GOODNEWS = "Καλά νέα";
	public static final String TITLE_OPINIONS = "Απόψεις";
	public static final String TITLE_CULTURE = "Πολιτισμός";
	public static final String TITLE_GREENLIFE = "Greenlife";
	public static final String TITLE_HISTORY = "Ιστορία";
	public static final String TITLE_HUMOR = "Χιούμορ";

	public static final String NEWS_FEED = "http://tvxs.gr/news.xml";
	public static final String GREECE_FEED = "http://tvxs.gr/news_greece.xml";
	public static final String WORLD_FEED = "http://tvxs.gr/news_world.xml";
	public static final String LOCAL_FEED = "http://tvxs.gr/news_local.xml";
	public static final String SCITECH_FEED = "http://tvxs.gr/news_scitech.xml";
	public static final String SPORT_FEED = "http://tvxs.gr/news_sports.xml";
	public static final String INTERNET_FEED = "http://tvxs.gr/news_internet.xml";
	public static final String GOODNEWS_FEED = "http://tvxs.gr/news_goodNews.xml";
	public static final String OPINIONS_FEED = "http://tvxs.gr/apopseis.xml";
	public static final String CULTURE_FEED = "http://tvxs.gr/politismos.xml";
	public static final String GREENLIFE_FEED = "http://tvxs.gr/greenlife.xml";
	public static final String HISTORY_FEED = "http://tvxs.gr/history.xml";
	public static final String HUMOR_FEED = "http://tvxs.gr/humor.xml";

	public static final String[] CATEGORIES_NAMES = {TITLE_NEWS,TITLE_GREECE,TITLE_WORLD,TITLE_LOCAL,TITLE_SCITECH,TITLE_SPORT,TITLE_INTERNET,TITLE_GOODNEWS,TITLE_OPINIONS,TITLE_CULTURE,TITLE_GREENLIFE,TITLE_HISTORY,TITLE_HUMOR};

	public static HashMap<String,String> CATEGORIES_MAP = new HashMap<String,String>();

	FeedURLS(){
		CATEGORIES_MAP.put(TITLE_NEWS, NEWS_FEED);
		CATEGORIES_MAP.put(TITLE_GREECE, GREECE_FEED);
		CATEGORIES_MAP.put(TITLE_WORLD, WORLD_FEED);
		CATEGORIES_MAP.put(TITLE_LOCAL, LOCAL_FEED);
		CATEGORIES_MAP.put(TITLE_SCITECH, SCITECH_FEED);
		CATEGORIES_MAP.put(TITLE_SPORT, SPORT_FEED);
		CATEGORIES_MAP.put(TITLE_INTERNET, INTERNET_FEED);
		CATEGORIES_MAP.put(TITLE_GOODNEWS, GOODNEWS_FEED);
		CATEGORIES_MAP.put(TITLE_OPINIONS, OPINIONS_FEED);
		CATEGORIES_MAP.put(TITLE_CULTURE, CULTURE_FEED);
		CATEGORIES_MAP.put(TITLE_GREENLIFE, GREENLIFE_FEED);
		CATEGORIES_MAP.put(TITLE_HISTORY, HISTORY_FEED);
		CATEGORIES_MAP.put(TITLE_HUMOR, HUMOR_FEED);
	}	
		
	public HashMap<String,String> englishTitles = new HashMap<String, String>();
}