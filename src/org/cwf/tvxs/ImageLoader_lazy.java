/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "A lazy image loader. Original code found at http://open-pim.com/tmp/LazyList.zip. Couldn't find  original author's contact details to ask for permission of using this code.If there is any problem relasing this with the GPLv3 please contact me at serko.apps@gmail.com"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
 */

package org.cwf.tvxs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Stack;
import android.os.Handler;
import android.os.Message;
import java.lang.Runnable;
import java.lang.Thread;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.widget.Toast;
import android.app.ProgressDialog;

public class ImageLoader_lazy implements Runnable {
	private static final String TAG = "LazyImageLoader";
	private static final int mThreshold = 50;
	public File[] files;
	public Context c;
	public Handler hd;

	private HashMap<String, Bitmap> cache=new HashMap<String, Bitmap>();

	private File cacheDir;

	public ImageLoader_lazy(Context context){
		c = context;
		//Make the background thead low priority. This way it will not affect the UI performance
		photoLoaderThread.setPriority(Thread.NORM_PRIORITY-1);

		//Find the dir to save cached images
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"Tvxs");
		else
			cacheDir=context.getCacheDir();
		if(!cacheDir.exists())
			cacheDir.mkdirs();

		files = cacheDir.listFiles();
	}

	final int stub_id=R.drawable.imageplaceholder;

	public Boolean needToCleanup(){
		if (files.length > mThreshold)
			return true;
		return false;
	}

	public void cleanup(){
		Thread thread  = new Thread(this);
		thread.start();
	}

	public void run(){
		for (int i = 0; i < files.length; i++){
			File f = files[i];
			try {
				Log.d(TAG,"delete:"+f);
				f.delete();
			}catch (Exception e){
				Log.d(TAG,"failed to delete file");
			}
		}
	}

	void DisplayImage(String url, Activity activity, ImageView imageView)
	{

		Log.d(TAG,"Load :"+url);

		if(cache.containsKey(url) && (cache.get(url)!=null)){
			Log.d(TAG, "Image is in cache:"+cache.get(url));
			imageView.setImageBitmap(cache.get(url));
		} else {
			Log.d(TAG,"Not in cache");
			queuePhoto(url, activity, imageView);
			imageView.setImageResource(stub_id);

		}    
	}

	private void queuePhoto(String url, Activity activity, ImageView imageView)
	{
		//This ImageView may be used for other images before. So there may be some old tasks in the queue. We need to discard them. 
		photosQueue.Clean(imageView);
		PhotoToLoad p=new PhotoToLoad(url, imageView);
		synchronized(photosQueue.photosToLoad){
			photosQueue.photosToLoad.push(p);
			photosQueue.photosToLoad.notifyAll();
		}

		//start thread if it's not started yet
		if(photoLoaderThread.getState()==Thread.State.NEW)
			photoLoaderThread.start();
	}

	private Bitmap getBitmap(String url) 
	{
		//identify images by hashcode. Not a perfect solution.
		if (url!=null){
			String filename=String.valueOf(url.hashCode());
			File f=new File(cacheDir, filename);
			// Log.d(TAG,"file:"+f+" with filename :"+filename);

			//from SD cache
			Bitmap b = decodeFile(f);
			if(b!=null){
				return b;
			}

			//from web
			try {
				// Log.d(TAG,"get from web:"+url);
				Bitmap bitmap=null;
				InputStream is=new URL(url).openStream();
				OutputStream os = new FileOutputStream(f);
				Utils.CopyStream(is, os);
				os.close();
				bitmap = decodeFile(f);
				return bitmap;
			} catch (Exception ex){
				ex.printStackTrace();
				Log.d(TAG,"ERROR:"+ex.toString());
				return null;
			}
		} else {
			Log.d(TAG,"URL IS NULL!!!");
		}
		return null;
	}

	//decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(File f){
		try {
			//decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f),null,o);

			//Find the correct scale value. It should be the power of 2.
			final int REQUIRED_SIZE=120;
			int width_tmp=o.outWidth, height_tmp=o.outHeight;
			int scale=1;
			while(true){
				if(width_tmp/2 < REQUIRED_SIZE || height_tmp/2 < REQUIRED_SIZE)
					break;
				width_tmp/=2;
				height_tmp/=2;
				scale*=2;
			}

			//decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize=scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {}
		return null;
	}

	//Task for the queue
	private class PhotoToLoad
	{
		public String url;
		public ImageView imageView;
		public PhotoToLoad(String u, ImageView i){
			url=u; 
			imageView=i;
		}
	}

	PhotosQueue photosQueue=new PhotosQueue();

	public void stopThread()
	{
		photoLoaderThread.interrupt();
	}

	//stores list of photos to download
	class PhotosQueue
	{
		private Stack<PhotoToLoad> photosToLoad=new Stack<PhotoToLoad>();

		//removes all instances of this ImageView
		public void Clean(ImageView image)
		{
			for(int j=0 ;j<photosToLoad.size();){
				if(photosToLoad.get(j).imageView==image)
					photosToLoad.remove(j);
				else
					++j;
			}
		}
	}

	class PhotosLoader extends Thread {
		public void run() {
			try {
				while(true)
				{
					//thread waits until there are any images to load in the queue
					if(photosQueue.photosToLoad.size()==0)
						synchronized(photosQueue.photosToLoad){
							photosQueue.photosToLoad.wait();
						}
					if(photosQueue.photosToLoad.size()!=0)
					{
						// Log.d(TAG,"loading images from queue");
						PhotoToLoad photoToLoad;
						synchronized(photosQueue.photosToLoad){
							photoToLoad=photosQueue.photosToLoad.pop();
						}
						Bitmap bmp=getBitmap(photoToLoad.url);
						cache.put(photoToLoad.url, bmp);
						Object tag=photoToLoad.imageView.getTag();
						if(tag!=null && ((String)tag).equals(photoToLoad.url)){
							BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad.imageView);
							Activity a=(Activity)photoToLoad.imageView.getContext();
							a.runOnUiThread(bd);
						}
					}
					if(Thread.interrupted())
						break;
				}
			} catch (InterruptedException e) {
				//allow thread to exit
			}
		}
	}

	PhotosLoader photoLoaderThread=new PhotosLoader();

	//Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable
	{
		Bitmap bitmap;
		ImageView imageView;
		public BitmapDisplayer(Bitmap b, ImageView i){bitmap=b;imageView=i;}
		public void run()
		{
			// Log.d(TAG,"RUN");
			if(bitmap!=null){
				Log.d(TAG,"set ImageView with bitmap:"+bitmap);
				imageView.setImageBitmap(bitmap);
			} else {
				Log.d(TAG,"Bitmap is null, using placeholder");
				imageView.setImageResource(stub_id);
			}
		}
	}

	public void clearCache() {
		//clear memory cache
		cache.clear();

		//clear SD cache
		File[] files=cacheDir.listFiles();
		for(File f:files)
			f.delete();
	}

}
