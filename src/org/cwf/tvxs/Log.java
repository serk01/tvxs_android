/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "a wrapper for calls to Log. can easily turn them on/off from here"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
*/

package org.cwf.tvxs;

public class Log {

    static final boolean LOG = true;

    public static void i(String tag, String string) {
	if (LOG) android.util.Log.i(tag, string);
    }
    public static void e(String tag, String string) {
	if (LOG) android.util.Log.e(tag, string);
    }
    public static void d(String tag, String string) {
	if (LOG) android.util.Log.d(tag, string);
    }
    public static void v(String tag, String string) {
	if (LOG) android.util.Log.v(tag, string);
    }
    public static void w(String tag, String string) {
	if (LOG) android.util.Log.w(tag, string);
    }
}