/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "A wrapper for each news object"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
*/

package org.cwf.tvxs;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


class NewsRowWrapper {
    View base;
    TextView title = null;
    TextView date = null;
    ImageView imageV = null;
  
    NewsRowWrapper(View base){
	this.base = base;
    }

    TextView getTitleTextView(){
	if (title == null)
	    title = (TextView)base.findViewById(R.id.rowTitle);

	return title;
    }

    TextView getDateTextView(){
	if (date == null)
	    date = (TextView)base.findViewById(R.id.rowDate);

	return date;
    }

    ImageView getImageView(){
	if (imageV == null)
	    imageV = (ImageView)base.findViewById(R.id.rowThumbnail);
    
	return imageV;
    }
}