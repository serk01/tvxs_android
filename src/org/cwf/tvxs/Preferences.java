/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "The preferences page"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
*/

package org.cwf.tvxs;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences;
import android.preference.CheckBoxPreference;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.util.Log;

public class Preferences extends PreferenceActivity {
    
    private static final String TAG = "Preferences";

    private CheckBoxPreference autoLoadFullArticles;
    private SharedPreferences prefs;
    private Editor prefsEditor;

    private Boolean hasPreferenceChanged = false;
    private Boolean firstCallToPrefs;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	addPreferencesFromResource(R.layout.preferences);

	//get preferences
	prefs = PreferenceManager.getDefaultSharedPreferences(this);

	//get Editor
	prefsEditor = prefs.edit();

	// // Set up a listener whenever a key changes            
        // getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

	//get auto load of articles checkbox instance
	autoLoadFullArticles = (CheckBoxPreference)getPreferenceScreen().findPreference(Constants.FullArticleAutoLoad_key);

	Log.d(TAG,"autoload:"+prefs.getBoolean(Constants.FullArticleAutoLoad_key,true));

	firstCallToPrefs = prefs.getBoolean("first_prefs_call",true);

	if (firstCallToPrefs){
	    Log.d(TAG,"first call to prefs:");
	    prefsEditor.putBoolean("first_prefs_call",false);
	    prefsEditor.putBoolean(Constants.FullArticleAutoLoad_key,true);
	    prefsEditor.commit();
	    autoLoadFullArticles.setChecked(true);
	}
    }
}