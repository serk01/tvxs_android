/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "Acceses the SharedPreferences objects used in the app and gets/sets their state"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
*/

package org.cwf.tvxs;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import java.lang.StringBuilder;
import java.util.StringTokenizer;
import java.util.ArrayList;
import android.content.Context;

public class SharedPrefsController{

    private Context ctx;
    private static final String TAG = "SharedPrefsController";

    private SharedPreferences prefs;
    private final Boolean DEBUG = false;
  
    SharedPrefsController(Context c){
	ctx = c;
	prefs = PreferenceManager.getDefaultSharedPreferences(ctx);

	if (DEBUG){
	    SharedPreferences.Editor pEdit = prefs.edit();
	    pEdit.remove(Constants.CATEGORIES_TAG);
	    pEdit.commit();
	}

    }

    public Boolean saveCategoriesToPrefs(ArrayList<String> activeCategories, Boolean changesMade){
	Boolean status;
	
	Log.d(TAG,"SAVE PREFERENCES");
	  
	//		create the active categories string
	StringBuilder activeCatString = new StringBuilder();
	for (String item : activeCategories){
	    activeCatString.append(item+",");
	}
		
		
	SharedPreferences.Editor pEdit = prefs.edit();
	pEdit.putString(Constants.CATEGORIES_TAG,activeCatString.toString());
	pEdit.putBoolean(Constants.HAS_CHANGED_FLAG,changesMade);
	status = pEdit.commit();	
	return status;
    }

    public Boolean hasPrefsChanged(){
    	return(prefs.getBoolean(Constants.HAS_CHANGED_FLAG,true));
    }

    public Boolean isFirstLaunch(){
	return(prefs.getBoolean(Constants.IS_FIRST_LAUNCH,true));
    }

    public void setFirstLaunchToFalse(){
	SharedPreferences.Editor pEdit = prefs.edit();
    	pEdit.putBoolean(Constants.IS_FIRST_LAUNCH,false);
    	pEdit.commit();
    }
    
    public void setPreferencesChanged(Boolean status){
    	SharedPreferences.Editor pEdit = prefs.edit();
    	pEdit.putBoolean(Constants.HAS_CHANGED_FLAG,status);
    	status = pEdit.commit();
    }

    public ArrayList<String> retrieveSavedCategoriesFromPrefs(){
    	ArrayList<String> activeCategories = new ArrayList<String>();
    	
    	//get active Categories
    	String activeCategs = prefs.getString(Constants.CATEGORIES_TAG,"");
	StringTokenizer activeCategSt = new StringTokenizer(activeCategs, ",");
	while (activeCategSt.hasMoreTokens()){
	    activeCategories.add(activeCategSt.nextToken());
	}
	Log.d(TAG,"activeCategories :"+activeCategs);
	
		
	return activeCategories;
    }
  
}