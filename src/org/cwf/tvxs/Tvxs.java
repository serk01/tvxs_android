/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "Application's entry point (i.e. main activity)
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
 */

package org.cwf.tvxs;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import android.app.AlertDialog;
import android.content.pm.PackageManager.NameNotFoundException;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnCancelListener;
import android.database.Cursor;
import android.view.ViewGroup.MarginLayoutParams;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import android.widget.Toast;

public class Tvxs extends TvxsBase implements AdapterView.OnItemClickListener, OnCancelListener

{

	private final String[] items1 = {"One","Two"};
	private final String[] items2 = {"Three", "Four"};
	private final String[] items3 = {"Five", "Six"};
	View.OnTouchListener mGestureListener;
	private ArrayList<String> mActiveCategories = new ArrayList<String>();
	private ArrayList<String[]> mUrlObjects = new ArrayList<String[]>();
	private AsyncTask mFeedParserThread;
	private AsyncTask mSaveAllThread;
	private Boolean mAllFeedsLoaded = false;
	private Boolean mInArticleDetail = false;
	private Boolean mIsFreshStart = true;
	private Boolean mIsRefreshing = false;
	private Context mContext;
	private FeedURLS feeds = new FeedURLS();
	private ProgressDialog mProgressDialog;
	private Scroller mScroller;
	private SharedPreferences mPrefs;
	private SharedPrefsController sharedPrefsCntr;
	private String TAG = "Tvxs";
	private final Boolean DEBUG = false;
	private int  mProgressTotal;
	private int currentlyLoadingPage;
	private int mActiveCategoryIndex = -1;
	private int screenWidth;
	private int failedRetries;
	private ImageLoader_lazy imageLoader;
	public ArrayList<ArrayList> collectionOfArticlesArrays = new ArrayList<ArrayList>(); //A collection of all the arrays of articles. Used to switch between set of Articles when changing page
	public ArrayList<ListView> collectionOfListViews = new ArrayList<ListView>(); //A Collection of all the ListViews
	public ArrayList<TvxsArticle> activeArrayOfArticles = new ArrayList<TvxsArticle>(); //the array of articles currently being used by the Adapter
	public ListView activeList;
	public RelativeLayout categoryContainer;
	public RelativeLayout contentContainer;
	public TvxsContentScroller contentScroller;
	public TvxsTitleScroller titleScroller;
	public int activePage = 0;
	public ProgressDialog cleanupProgressDialog;
	private TvxsDbAdapter dbAdapter;
	private boolean mLoadOffline = true;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{

		mContext = getApplicationContext();
		//savedInstanceState causes the app to crash in some scenarios. I won't worry with it for now and just set it to null

		savedInstanceState = null;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		failedRetries = 0;
		sharedPrefsCntr = new SharedPrefsController(this); 
		imageLoader = new ImageLoader_lazy(this.getApplicationContext());
		if (imageLoader.needToCleanup()){
			Log.d(TAG,"CLEANUP-------------------------------");
			// Toast.makeText(this , getResources().getString(R.string.cleanup), Toast.LENGTH_LONG).show();
			imageLoader.cleanup();
		}

		Display display = getWindowManager().getDefaultDisplay(); 
		screenWidth = display.getWidth();

		createMainLayout();

		//setup the refresh button
		ImageButton btnRefresh = (ImageButton)findViewById(R.id.btnRefresh);
		int refreshHeight = btnRefresh.getLayoutParams().height;
		int refreshWidth = btnRefresh.getLayoutParams().width;

		ImageView logo = (ImageView)findViewById(R.id.logo);
		int logoHeight = logo.getLayoutParams().height;

		ImageButton btnSaveAll = (ImageButton)findViewById(R.id.btnSave);


		((MarginLayoutParams)btnRefresh.getLayoutParams()).leftMargin = screenWidth - (refreshWidth + refreshWidth/2);
		((MarginLayoutParams)btnRefresh.getLayoutParams()).topMargin = logoHeight/2 - refreshHeight/2;


		btnRefresh.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v){
				if (isNetworkUp()){
					mLoadOffline = false;
					mIsRefreshing = true;
					resetPage();
					composeListOfActiveFeedsAndStartParsing();
					mIsRefreshing = false;
				}
			}
		});

		btnSaveAll.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mSaveAllThread = new saveAll().execute();

			}
		});

	}

	private class saveAll extends AsyncTask {

		//	private class parseFeedsInBackground extends AsyncTask<ArrayList, ArrayList, Integer> {

		@Override
		protected Integer doInBackground(Object... params) {
			dbAdapter.open();
			int size = collectionOfArticlesArrays.size();
			Integer count = 0;
			for (int i=0; i<size; i++)
			{
				ArrayList articlesArray = collectionOfArticlesArrays.get(i);
				int arraySize = articlesArray.size();
				for (int j=0; j<arraySize; j++)
				{
					TvxsArticle article = (TvxsArticle) articlesArray.get(j);
					try {
						long ret = dbAdapter.saveArticleOffline(article.category, article);
						if (ret != -1)
						{
							count+=1;
							Log.d(TAG,"Succesfuly saved article "+ article.getTitle() + " offline");
						}
						else
							Log.d(TAG,"Failed to save article "+ article.getTitle() + " offline");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			return count;


		}

		@Override
		protected void onProgressUpdate(Object... progress){

//			if (count == size-1)
//				Toast.makeText(this, "All saved. Great success!", Toast.LENGTH_LONG).show();
//			else
//				Toast.makeText(this, "Some articles could not be saved", Toast.LENGTH_LONG).show();
			dbAdapter.close();			
		}

		protected void onPostExecute(Long result){

		}



	}



	private void parseOfflineFeeds()
	{
		dbAdapter.open();

		int size = mUrlObjects.size();
		ArrayList<ArrayList<TvxsArticle>> articlesArray = new ArrayList<ArrayList<TvxsArticle>>();
		ArrayList pubObject = new ArrayList();

		for (int i=0; i<size; i++){
			Log.d(TAG,"saved table "+mUrlObjects.get(i)[1]);
			String table = mUrlObjects.get(i)[1];
			Cursor c = dbAdapter.getOfflineArticlesForCategory(Utils.cleanCategoryString(table));
			Log.d(TAG,"items in db "+c.getCount());
			ArrayList<TvxsArticle> catArticles = new ArrayList<TvxsArticle>();

			while (c.moveToNext())
			{
				String title = c.getString(c.getColumnIndex(Constants.KEY_TITLE));				
				String teaser = c.getString(c.getColumnIndex(Constants.KEY_TEASER));
				String articleUrl = c.getString(c.getColumnIndex(Constants.KEY_ARITCLE_URL));
				String pubDate = c.getString(c.getColumnIndex(Constants.KEY_PUBDATE));
				String imgUrl = c.getString(c.getColumnIndex(Constants.KEY_IMGURL));
				TvxsArticle tvxsArticle = new TvxsArticle(title, teaser, imgUrl, articleUrl, pubDate, table);
				catArticles.add(tvxsArticle) ; //get all the articles and add them to the articlesArra
			}

			pubObject.add(catArticles);
			pubObject.add(table);
			collectionOfArticlesArrays.add(catArticles);
		}
		loadArticleListView(pubObject);
		dbAdapter.close();

	}


	private Boolean isNetworkUp(){
		if (Utils.isWifiAvailable(this))
			return true;
		else
			Toast.makeText(this, getResources().getString(R.string.nonetwork), Toast.LENGTH_LONG).show();
		return false;
	}


	@Override
	protected void onResume(){
		super.onResume();
		dbAdapter = new TvxsDbAdapter(this);
		Log.d(TAG,"--------------ON RESUME------------------");

		if (!mLoadOffline)
		{
			if (isNetworkUp()){
				if (!mInArticleDetail)
					composeListOfActiveFeedsAndStartParsing();
				else {
					mInArticleDetail = false;
					contentScroller.scrollToPage(activePage);
				}
			}
		} else 
		{
			resetPage();
			composeListOfActiveFeedsAndStartParsing();
			parseOfflineFeeds();
		}
	}

	private void showHelpDialog(){
		if (sharedPrefsCntr.isFirstLaunch()){
			try {
				AlertDialog builder;
				builder = AboutDialog.create(this,false);
				builder.show();
			} catch(NameNotFoundException e){
				e.printStackTrace();
			}
			sharedPrefsCntr.setFirstLaunchToFalse();
		}
	}

	private void getPrefsAndPopulateUrlsToParse(){

		mUrlObjects.clear();
		ArrayList<String> activeCategories = sharedPrefsCntr.retrieveSavedCategoriesFromPrefs();
		if (activeCategories.size()>0){
			for (String catTitle : activeCategories){
				String[] url = new String[] { FeedURLS.CATEGORIES_MAP.get(catTitle), catTitle};
				Log.d(TAG,"TITLE:"+catTitle);
				Log.d(TAG,"URL:"+FeedURLS.CATEGORIES_MAP.get(catTitle));
				mUrlObjects.add(url);
			}
			//if first installation there will be no active categories saved so use the following default categories	    
		} else { 

			String[] url1 = new String[] { FeedURLS.NEWS_FEED, FeedURLS.TITLE_NEWS};
			String[] url2 = new String[] { FeedURLS.GREECE_FEED, FeedURLS.TITLE_GREECE};
			String[] url3 = new String[] { FeedURLS.WORLD_FEED, FeedURLS.TITLE_WORLD};

			mUrlObjects.add(url1);
			mUrlObjects.add(url2);
			mUrlObjects.add(url3);
		}

		mActiveCategories = activeCategories;
		sharedPrefsCntr.setPreferencesChanged(false); //reset this flag
	}

	private void composeListOfActiveFeedsAndStartParsing(){

		if (!DEBUG){
			// prepare and execute URL parsing
			try{
				Log.d(TAG,"PREFERENCES CHANGED:"+sharedPrefsCntr.hasPrefsChanged());
				// if (sharedPrefsCntr.hasPrefsChanged()){
				//     resetPage();
				// }
				if (!sharedPrefsCntr.hasPrefsChanged() && !mIsFreshStart && !mIsRefreshing){ //if preferences have not changed
					Log.d(TAG,"NOTHING HAS CHANGED. JUST REDRAW PREVIOUS ACTIVITY");
					if (mAllFeedsLoaded == true){
						Log.d(TAG, "LAST KNOW ACTIVE PAGE :"+activePage);
						contentScroller.scrollToAppropriatePage();
					}
				}
				else if (sharedPrefsCntr.hasPrefsChanged() || mIsFreshStart || mIsRefreshing) {
					resetPage();
					mIsFreshStart = false;
					getPrefsAndPopulateUrlsToParse();
					if (!mLoadOffline)
						startParserThread();
				}
			} catch (Exception e){
				Log.e(TAG, e.getMessage());
			}
		} else {
			addNewListView(1);
			addNewListView(2);
			addNewListView(3);
			addTitleToCategoryContainer("Nea",1);
			addTitleToCategoryContainer("Kosmos",2);
		}

	}

	private void startParserThread(){
		mFeedParserThread = new parseFeedsInBackground().execute(mUrlObjects);
		showProgressDialog();

	}

	private void resetPage(){
		activeArrayOfArticles.clear();
		collectionOfArticlesArrays.clear();
		collectionOfListViews.clear();
		titleScroller.removeAllTitles();
		categoryContainer.removeAllViews();
		contentContainer.removeAllViews();
		mActiveCategoryIndex = -1;
		mAllFeedsLoaded = false;

	}



	private void createMainLayout(){

		//get the mainContainer from the main.xml
		RelativeLayout mainContainer = (RelativeLayout)findViewById(R.id.linearContainer);


		//create the RelativeLayout that will contain the ListViews with the main news content
		contentContainer = createNewContentContainer();	

		//create the Content scroller
		contentScroller = new TvxsContentScroller(getApplicationContext(),this);

		//create the Category names scroller
		titleScroller = new TvxsTitleScroller(getApplicationContext(),contentScroller,screenWidth);

		//create the RelativeLayout that will contain the Category names
		categoryContainer = createNewCategoryContainer();

		//add each RelativeLayout to its corresponding HorizontalScrollView
		// titleScroller.addView(categoryContainer);
		contentScroller.addView(contentContainer);

		//add the HorizontalScrollViews to the main view's LinearLayout

		mainContainer.addView(titleScroller);
		mainContainer.addView(contentScroller,contentScroller.getLayoutParams());

	}

	private void addTitleToCategoryContainer(String title, int id){
		// TextView txtV = new TextView(this);
		Button btnTitle = new Button(this);
		btnTitle.setPadding(0, 5, 10, 5);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, Constants.TITLE_SCROLLER_HEIGHT);

		params.addRule(RelativeLayout.RIGHT_OF, id-1);

		btnTitle.setId(id);
		btnTitle.setTextSize(23);
		btnTitle.setTextColor(Color.GRAY);
		btnTitle.setLayoutParams(params);
		btnTitle.setText(title);
		btnTitle.setBackgroundColor(Color.TRANSPARENT);
		categoryContainer.addView(btnTitle,params);
		btnTitle.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v){
				Log.d(TAG,"TAPPED ON BUTTON WITH ID :"+v.getId());
				contentScroller.scrollToPage(v.getId()-1);
			}
		});
	}

	private void addNewListView(int index){

		ListView tmpList = new ListView(this);
		tmpList.setPadding(5, 0, 5, 0);
		tmpList.setDividerHeight(1);
		tmpList.setOnItemClickListener(this);
		tmpList.setId(index);
		Log.d(TAG,"tmpList ID:"+Integer.toHexString(index));
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.FILL_PARENT);
		params.addRule(RelativeLayout.RIGHT_OF,index-1);

		tmpList.setLayoutParams(params);
		tmpList.getLayoutParams().width = screenWidth;
		contentContainer.addView(tmpList,params);

		activeList = tmpList;
		collectionOfListViews.add(tmpList);

		if (DEBUG){
			String[] activeItems = {};
			if (index==0)
				activeItems = items1;
			else if (index == 1)
				activeItems = items2;
			else if (index == 2)
				activeItems = items3;

			tmpList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, activeItems));
		}
		else{
			Log.d(TAG,"CREATE NEW ADAPTER");
			HomeListAdapter hla = new HomeListAdapter();
			hla.setActivity(this);
			activeList.setAdapter(hla);
		}
	}

	private RelativeLayout createNewContentContainer(){
		RelativeLayout r = new RelativeLayout(this);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.FILL_PARENT);
		r.setLayoutParams(params);
		return r;
	}

	private RelativeLayout createNewCategoryContainer(){
		RelativeLayout r = new RelativeLayout(this);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, Constants.TITLE_SCROLLER_HEIGHT);
		// RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(screenWidth*2,CATEGORY_SCROLLER_HEIGHT);	
		r.setLayoutParams(params);
		return r;
	}

	public void onCancel(DialogInterface dialog){
		mProgressDialog.dismiss();
		Log.d(TAG,"Request canceled");
		mHandler.sendEmptyMessage(0);
		mFeedParserThread.cancel(true);
		finish();
	}

	public void showProgressDialog(){

		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setCancelable(true);
		mProgressDialog.setOnCancelListener(this);
		mProgressDialog.setMessage(getString(R.string.download_msg));
		// set the progress to be horizontal
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		// reset the bar to the default value of 0
		mProgressDialog.setProgress(0);
		// set the maximum value
		mProgressDialog.setMax(mActiveCategories.size());
		// display the progressbar
		mProgressDialog.show();
	}


	final Handler mHandler = new Handler(){
		public void handleMessage(Message msg){
			//get the current variable total from the message and 
			//update the progress bar
			int result = msg.getData().getInt("result");
			int action = msg.getData().getInt("action");

			if (action == Constants.MINUSONEITEM)
				mProgressDialog.setMax(mProgressDialog.getMax()-1);

			if (result == Constants.FAIL)
				mProgressDialog.dismiss();
			else {
				// mProgressDialog.setProgress(total);
				mProgressDialog.incrementProgressBy(1);
				if (mProgressDialog.getProgress() >= mProgressDialog.getMax())
					mProgressDialog.dismiss();
			}
		}
	};

	public void loadArticleListView(ArrayList incomingArticlesArray)
	{
		int listID = 1000; //start from a high enough number to avoid conflicts
		int size = incomingArticlesArray.size();
		for (int j=0; j < size;j+=2){
			activeArrayOfArticles = (ArrayList<TvxsArticle>)incomingArticlesArray.get(j); //return an Array of TvxsArticles and assign it active
			addNewListView(listID);
			String title = (String)incomingArticlesArray.get(j+1);
			titleScroller.addTitleToHorizontalScroller(title,getApplicationContext());
			listID++;
		}

		//reset scrollers to first pae
		contentScroller.scrollToPage(0);
		titleScroller.setSelectedTitleAtIndex(0);
	}

	private class parseFeedsInBackground extends AsyncTask<ArrayList, ArrayList, Integer> {
		@Override
		protected Integer doInBackground(ArrayList... urlsList){
			Integer size = 0;
			ArrayList feedsToParse = urlsList[0];

			Log.d(TAG,"FEEDS TO PARSE:----->"+feedsToParse);
			try {
				/* Get a SAXParser from the SAXPArserFactory. */
				SAXParserFactory spf = SAXParserFactory.newInstance();
				SAXParser sp = spf.newSAXParser();

				/* Get the XMLReader of the SAXParser we created. */
				XMLReader xr = sp.getXMLReader();

				/* Parse the xml-data from our URL. */
				int numberOfFeeds = feedsToParse.size();

				ArrayList pubObject = new ArrayList();

				mProgressDialog.setMax(numberOfFeeds);
				for (int i = 0; i < numberOfFeeds; i++){
					String[] urlObject = (String[])feedsToParse.get(i);
					URL urlToParse = new URL(urlObject[0]); // FeedURLS have the url at index 0
					String categoryToParse = urlObject[1];

					/* Create a new ContentHandler and apply it to the XML-Reader*/
					//TODO: Change this so as to create one instance only"
					Log.d(TAG,"category to parse:"+ categoryToParse);
					TvxsRssHandler myRssHandler = new TvxsRssHandler(categoryToParse);
					xr.setContentHandler(myRssHandler);

					Log.d(TAG, "******************* PARSING FEED FROM " + urlToParse + "********************");

					try {
						xr.parse(new InputSource(urlToParse.openStream()));

						/* Parsing has finished. */
						ArrayList<TvxsArticle> articlesArray = new ArrayList<TvxsArticle>();
						articlesArray.addAll(myRssHandler.getParsedArticlesListUpToIndex(0)); //get all the articles and add them to the articlesArra

						pubObject.add(articlesArray); //add the Array of articles from the last parsed feed to the array of published objects
						pubObject.add(urlObject[1]); //and add its title as the immediate object after the array. So each feed gets two entries in the array.
						//		    activeArrayOfArticles = articlesArray; // assign the current array of articles to the active array to be used from the Adapter
						collectionOfArticlesArrays.add(articlesArray); //add this array of articles to the list that will be used to switch from one set to another when page changes
						Log.d(TAG,"PARSED FEED :"+urlToParse);

						mHandler.sendEmptyMessage(0);
					}catch (Exception e){
						Log.e(TAG,"Url parsing error:"+e);

						//notify progress dialog that there is one item less 
						Message msg = new Message();
						Bundle b = new Bundle();
						b.putInt("action",Constants.MINUSONEITEM);
						msg.setData(b);
						mHandler.sendMessage(msg);
					}
				}
				publishProgress(pubObject);	
				mAllFeedsLoaded = true;
				// contentScroller.scrollToAppropriatePage();
			} catch (Exception e){
				Log.e(TAG, "Error:"+e);
				Message msg = new Message();
				Bundle b = new Bundle();
				b.putInt("result",Constants.FAIL);
				msg.setData(b);
				mHandler.sendMessage(msg);
			}
			return size;
		}



		@Override
		protected void onProgressUpdate(ArrayList... incomingObj){
			Log.d(TAG,"PROGRESS UPDATE");
			ArrayList incomingArticlesArrays = incomingObj[0];
			loadArticleListView(incomingArticlesArrays);

			//			int listID = 1000; //start from a high enough number to avoid conflicts
			//			int size = incomingArticlesArrays.size();
			//			for (int j=0; j < size;j+=2){
			//				activeArrayOfArticles = (ArrayList<TvxsArticle>)incomingArticlesArrays.get(j); //return an Array of TvxsArticles and assign it active
			//				addNewListView(listID);
			//				String title = (String)incomingArticlesArrays.get(j+1);
			//				titleScroller.addTitleToHorizontalScroller(title,getApplicationContext());
			//				listID++;
			//			}
			//
			//			//reset scrollers to first pae
			//			contentScroller.scrollToPage(0);
			//			titleScroller.setSelectedTitleAtIndex(0);

		}

		protected void onPostExecute(Integer size){
			if (mAllFeedsLoaded == true){
				// Toast.makeText(getApplicationContext(),getResources().getString(R.string.loadsuccess), Toast.LENGTH_LONG).show();
				showHelpDialog();
			}  else {
				//retry
				if (failedRetries<3){
					failedRetries+=1;
					startParserThread();
				} else {
					Toast.
					makeText(getApplicationContext(),getResources().getString(R.string.loadfail), Toast.LENGTH_LONG)
					.show();
				}
			}
		}
	}


	private TvxsArticle getListModel(int position) {

		return(((HomeListAdapter)activeList.getAdapter()).getItem(position));
	}


	public void onItemClick(AdapterView<?>  parent, View  view, int position, long id){
		if (!DEBUG){
			Log.d(TAG,"ACTIVE PAGE :"+activePage);
			Log.d(TAG,"CollectionOFArticles:"+collectionOfArticlesArrays);
			activeArrayOfArticles = collectionOfArticlesArrays.get(activePage);
			final String articleDesc = activeArrayOfArticles.get(position).getTeaser();

			Intent i = new Intent(this, TvxsArticleDetailActivity.class);
			i.putExtra("title", activeArrayOfArticles.get(position).getTitle());
			i.putExtra("teaser", activeArrayOfArticles.get(position).getTeaser());
			i.putExtra("url",activeArrayOfArticles.get(position).getArticleUrl());
			i.putExtra("pubdate",activeArrayOfArticles.get(position).getPubDate());
			i.putExtra("imgurl",activeArrayOfArticles.get(position).getImageUrl());
			startActivity(i);
			mInArticleDetail = true;

		}
	}

	private class HomeListAdapter extends ArrayAdapter<TvxsArticle> {

		public Activity activity;

		HomeListAdapter() {
			super(getApplicationContext(), R.layout.simple_row, activeArrayOfArticles);
		}

		public void setActivity(Activity a){
			activity = a;
		}

		public View getView(int position, View convertView, ViewGroup parent){

			View row = convertView;

			NewsRowWrapper wrapper = null;
			//get the appropriate list view based on the parent view (i.e. the Listview being currently drawn.
			//There must be a better way (i.e. more correct/efficient way of doing this!)
			activeList = (ListView)collectionOfListViews.get(collectionOfListViews.indexOf(parent));


			if (row == null){
				LayoutInflater inflater = getLayoutInflater();
				row = inflater.inflate(R.layout.simple_row, parent, false);
				wrapper = new NewsRowWrapper(row);
				row.setTag(wrapper);
				row.setId(activePage);
			} else {
				wrapper = (NewsRowWrapper)row.getTag();
			}

			TvxsArticle article = getListModel(position);

			String title = article.getTitle();
			String date = article.getPubDate();
			String imageUrl = article.getImageUrl();
			wrapper.getTitleTextView().setText(title);
			wrapper.getDateTextView().setText(date);
			ImageView imageView = wrapper.getImageView();
			imageView.setTag(imageUrl);
			// imageLoader.fetchDrawableOnThread(imageUrl,wrapper.getImageView());
			Log.d(TAG,"image url:"+imageUrl);
			if (imageUrl!=null)
				imageLoader.DisplayImage(imageUrl, activity, imageView);
			else {
				Log.d(TAG,"empty image");
				imageView.setImageResource(R.drawable.imageplaceholder);
			}
			return row;

		}
	}

}







