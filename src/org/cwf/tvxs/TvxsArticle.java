/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "The TvxsArticle class"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
 */

package org.cwf.tvxs;


class TvxsArticle extends Object {
	public String title;
	public String description;
	public String imageUrl;
	public String articleUrl;
	public String pubDate;
	public String category;

	//Constructor
	public TvxsArticle(String title,
			String desc,
			String imageUrl,
			String articleUrl,
			String pubDate,
			String category){

		this.title = title;
		this.description = desc;
		this.imageUrl = imageUrl;
		this.articleUrl = articleUrl;
		this.pubDate = pubDate;
		this.category = category;
	}

	public String getTitle(){
		return this.title;
	}

	public String getTeaser(){
		return this.description;
	}

	public String getArticleUrl(){
		return this.articleUrl;
	}

	public String getPubDate(){
		return this.pubDate;
	}

	public String getImageUrl(){
		return this.imageUrl;
	}
	
	public String getCategory(){
		return this.category;
	}
}