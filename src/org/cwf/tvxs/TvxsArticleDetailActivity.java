/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "The article's detail activity. Accesed when user taps on a listview row"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
 */

package org.cwf.tvxs;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.KeyEvent;
import android.view.Display;
import android.app.ProgressDialog;
import android.widget.TextView;
import android.widget.ScrollView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.LinearLayout;
import android.widget.AbsoluteLayout;
import android.graphics.drawable.BitmapDrawable;
import android.widget.Toast;
import android.content.Context;
import android.view.LayoutInflater;
import android.content.Intent;
import android.text.method.ScrollingMovementMethod;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.text.Html;

import java.io.IOException;
import java.lang.StringBuilder;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View.OnClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.PopupWindow;
import android.app.Activity;
import android.view.Window;
import android.view.ViewGroup;
import android.view.Gravity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebChromeClient;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;

public class TvxsArticleDetailActivity extends Activity{

	private String TAG = "TvxsArticleDetail";
	private TextView txtTitle;
	private TextView txtTeaser;
	private TextView txtPubDate;
	//    private TextView txtUrlLink;
	private ImageView articleImageView;
	private Button btnMobilize;
	private Button btnShare;
	private Button btnSave;

	private StringBuilder urlText;
	private PopupWindow mPopupWindow;
	private WebView mWebView;
	private View mPopupLayout;
	private RelativeLayout mParentContainer;
	private Boolean autoLoadArticles;
	private TvxsDbAdapter dbAdapter; 

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_PROGRESS);
		dbAdapter = new TvxsDbAdapter(getApplicationContext());

		//get preferences
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		autoLoadArticles = prefs.getBoolean(Constants.FullArticleAutoLoad_key,true);
		Log.d(TAG,"LOAD FULL ARTICLES :"+autoLoadArticles);

		setContentView(R.layout.article_detail_view);
		mParentContainer = (RelativeLayout)findViewById(R.id.article_detail_container);

		Display display = getWindowManager().getDefaultDisplay();
		int screenWidth = display.getWidth();

		Intent intent = getIntent();
		txtTitle = (TextView)findViewById(R.id.articleTitle);
		txtTeaser = (TextView)findViewById(R.id.teaser);
		txtPubDate = (TextView)findViewById(R.id.pubDate);
		btnMobilize = (Button)findViewById(R.id.btnUrlLink);
		articleImageView = (ImageView)findViewById(R.id.articleImg);
		btnShare = (Button)findViewById(R.id.btnShare);
		btnSave = (Button)findViewById(R.id.btnSave);

		int screenHalf = screenWidth / 2;

		int width = ((MarginLayoutParams)btnMobilize.getLayoutParams()).width;

		int btnMobilizeX = screenHalf - width - 10 ;

		((MarginLayoutParams)btnMobilize.getLayoutParams()).leftMargin = btnMobilizeX;

		int btnShareX = ((MarginLayoutParams)articleImageView.getLayoutParams()).rightMargin - btnShare.getWidth();
		((MarginLayoutParams)btnMobilize.getLayoutParams()).leftMargin = btnMobilizeX;


		txtTeaser.setMovementMethod(ScrollingMovementMethod.getInstance()); //to make the TextView scrollable

		final String title = intent.getExtras().getString("title");
		final String teaser = intent.getExtras().getString("teaser");
		final String pubDate = intent.getExtras().getString("pubdate");	
		final String url = intent.getExtras().getString("url");
		final String imgurl  = intent.getExtras().getString("imgurl");
		final byte[] imageblob = intent.getExtras().getByteArray("imageblob");

		btnShare.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v){
				final Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_SUBJECT,getResources().getString(R.string.sharemsg));
				intent.putExtra(Intent.EXTRA_TEXT, url);
				startActivity(Intent.createChooser(intent, getResources().getString(R.string.sharetitle)));
			}
		});

		btnSave.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Log.d(TAG,"Save");
				btnSave.setBackgroundDrawable(getResources().getDrawable(R.drawable.yellowstar));
				try {
					saveFavourite(title,teaser, pubDate, imgurl, url, urlText.toString());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		//set the star icon based on the items saved status
		dbAdapter.open();
		setStarImage(title, teaser);
		dbAdapter.close();

		txtTitle.setText(title);
		txtTeaser.setText(teaser);
		txtPubDate.setText(pubDate);

		if (imageblob != null)
		{
			articleImageView.setImageBitmap(BitmapFactory.decodeByteArray(imageblob, 0, imageblob.length));
		} else if (imgurl !=null)
		{
			ImageLoader_lazy imageLoader = new ImageLoader_lazy(this.getApplicationContext());
			articleImageView.setTag(imgurl);
			imageLoader.DisplayImage(imgurl, this, articleImageView);
		} else 
			articleImageView.setImageResource(R.drawable.imageplaceholder);

		//create the mobilized url
		urlText = new StringBuilder();
		urlText.append("http://www.google.com/gwt/x?u=");
		urlText.append(url);
		urlText.append("&btnGo=Go&source=wax&ie=UTF-8&oe=UTF-8");
		//	txtUrlLink.setMovementMethod(LinkMovementMethod.getInstance()); //to make the link clickable
		Log.d(TAG,"urlText ="+urlText.toString());

		// Prefetch the complete url article (mobilized format) here in case user wants to see full article. 
		// Note:Although this uses more bandwithd that necessary, many users complained that the full articles were not accessible.
		// Hence showing the full article in a webview (and not in a browser) as fast as possible could satisify users instead.

		//We need to get the instance of the LayoutInflater, use the context of this activity
		LayoutInflater inflater = (LayoutInflater) TvxsArticleDetailActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		//Inflate the view from a predefined XML layout
		mPopupLayout = inflater.inflate(R.layout.webpopup,(ViewGroup)findViewById(R.id.popupLayout));	    
		//get the webview

		mWebView = (WebView)mPopupLayout.findViewById(R.id.webview_content);
		mWebView.setWebViewClient(new WebClient());
		mWebView.getSettings().setJavaScriptEnabled(true);

		final Activity activity = this;
		mWebView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				activity.setTitle("Φόρτωση πλήρους άρθρου...");
				// Activities and WebViews measure progress with different scales.
				// The progress meter will automatically disappear when we reach 100%
				activity.setProgress(progress * 100);
				if (progress == 100)
					activity.setTitle("Tvxs");
			}
		});

		if (autoLoadArticles)
			mWebView.loadUrl(urlText.toString()); //preload url link (mobilized)



		btnMobilize.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// final Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse(urlText.toString()));
				// startActivity(browserIntent);
				if (!autoLoadArticles){
					mWebView.loadUrl(urlText.toString()); //load article explicitly
				}
				showPopup(title, pubDate);
			}
		});
	}

	private void readAllFavourites()
	{
		Cursor c = dbAdapter.fetchAllFavourites();
		Log.d(TAG,"items in db "+c.getCount());

		while (c.moveToNext())
		{
			Log.d(TAG, c.getString(c.getColumnIndex("title")));
		}

	}

	//if article is not saved it saves it, otherwise it deletes it
	private void saveFavourite(String title, String teaser, 
			String pubdate, String imgurl, 
			String articleurl, String fullarticle) throws IOException 
			{
		dbAdapter.open();
		long result = dbAdapter.createFavourite(title, teaser, 
				pubdate, imgurl, 
				articleurl, fullarticle);
		if (result != -1)
			Log.d(TAG,"favourite created with Title "+ title +" and content "+teaser);
		else
		{
			Log.d(TAG,"favourite already exists");
			dbAdapter.deleteFavourite(title);

		}
		setStarImage(title,teaser);
		dbAdapter.close();
			}

	private void setStarImage(String title, String teaser)
	{
		boolean articleExists = dbAdapter.articleExists(title);
		if (articleExists)
			btnSave.setBackgroundDrawable(getResources().getDrawable(R.drawable.yellowstar));
		else
			btnSave.setBackgroundDrawable(getResources().getDrawable(R.drawable.whitestar));
	}


	private class WebClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;

		}
	}

	private void showPopup(String title, String date){
		try {
			Display display = getWindowManager().getDefaultDisplay(); 
			int screenWidth = display.getWidth();
			int screenHeight = display.getHeight();
			int width = screenWidth;
			int height = screenHeight;
			int[] loc = new int[2]; 
			txtPubDate.getLocationOnScreen(loc);
			int lastWidgetsYcoord = loc[1];
			int lastWidgetsHeight = txtPubDate.getHeight();
			mPopupWindow = new PopupWindow(mPopupLayout, width, height, true);
			mPopupWindow.setBackgroundDrawable(new BitmapDrawable()); //necessary inorder for the PopupWindow to respond to key down events

			//display the popup window
			mPopupWindow.showAtLocation(mPopupLayout, Gravity.CLIP_VERTICAL, 0, lastWidgetsYcoord + lastWidgetsHeight);

			// ImageButton closeButton = (ImageButton)mPopupLayout.findViewById(R.id.webview_close);

			// closeButton.setOnClickListener(new View.OnClickListener() {
			// 	    public void onClick(View v) {
			// 		System.out.println("dismiss webview");
			// 		mPopupWindow.dismiss();
			// 	    }
			// 	});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume(){
		super.onResume();
	}

	@Override
	protected void onPause(){
		super.onPause();
		dbAdapter.close();
	}

	@Override
	protected void onStop(){
		super.onStop();
		dbAdapter.close();
	}

}
