/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "A base class that is inherited by other classes.Provides access to the menu"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
*/

package org.cwf.tvxs;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.app.AlertDialog;
import android.content.pm.PackageManager.NameNotFoundException;

public class TvxsBase extends Activity {
  

    @Override
	public void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);

    }

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
	new MenuInflater(this).inflate(R.menu.mainmenu, menu);
	return(super.onCreateOptionsMenu(menu));
    }  


    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
	return(applyMenuChoice(item) ||
	       super.onOptionsItemSelected(item));
    }
    
    private boolean applyMenuChoice(MenuItem item) {
	AlertDialog builder = null;
	switch (item.getItemId()) {

	case R.id.categories:
	    System.out.println("OPTIONS TAPPED");
	    Intent categories = new Intent(this, Categories.class);
	    startActivity(categories);
	    return(true);
	case R.id.preferences:
	    Intent prefs = new Intent(this, Preferences.class);
	    startActivity(prefs);
	    return(true);
	case R.id.favourites:
		Intent favs = new Intent(this, FavouritesActivity.class);
		startActivity(favs);
		return(true);
	case R.id.about:
	    try {
		builder = AboutDialog.create(this,true);
		builder.show();
	    } catch(NameNotFoundException e){
		e.printStackTrace();
	    }
	    return(true);
	case R.id.help:
	    try {
		builder = AboutDialog.create(this,false);
		builder.show();
	    } catch(NameNotFoundException e){
		e.printStackTrace();
	    }
	    return(true);
	}
	return(false);
    }
}