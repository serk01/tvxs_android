/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "The class that handles the interactions with the listviews.Swiping around the listviews is handled here"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
*/

package org.cwf.tvxs;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.RelativeLayout;


public class TvxsContentScroller extends HorizontalScrollView {

    RelativeLayout contentContainer;
    Tvxs parent;
    Boolean isGoingRight;
    Boolean isGoingLeft;
    double oldX = 0.0;
    private static final String TAG = "TvxsContentScroller";
    
    TvxsContentScroller(Context context, Tvxs p){
	super(context);
	parent = p;
	RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
									     RelativeLayout.LayoutParams.FILL_PARENT);
	params.addRule(RelativeLayout.BELOW, Constants.TITLE_SCROLLER);
	this.setLayoutParams(params);
      
	// set the TouchListener
	isGoingRight = false;
	isGoingLeft = false;
	OnTouchListener mGestureListener =  new View.OnTouchListener() {
		public boolean onTouch(View v, MotionEvent event) {

		    
		    if (oldX !=0 ){
			if (event.getX() < oldX){
			    isGoingRight = true;
			    isGoingLeft = false;
			}
			else if (event.getX() > oldX){
			    isGoingLeft = true;
			    isGoingRight = false;
			}
		    }
		    oldX = event.getX();
		    //check when the touch has been removed
		    if (event.getAction() == MotionEvent.ACTION_UP){

		    	scrollToAppropriatePage();
			return true;
		    }
		    return false;
		}
	    };

	this.setOnTouchListener(mGestureListener);
    }

    public void scrollToAppropriatePage(){
	double currentPosition = this.getScrollX();
	double pagesCount = parent.contentContainer.getChildCount();
	double pageLengthInPx = parent.contentContainer.getMeasuredWidth()/pagesCount;
	double currentPage = currentPosition/pageLengthInPx;
	double threshold = 0.0;
	Boolean changePage = false;
		
	if (isGoingRight)
	    threshold = 0.25;
	else if (isGoingLeft)
	    threshold = 0.75;

	changePage = currentPage-(int)currentPage > threshold;

	double edgePosition = 0.0;
		    
	if(changePage)
	    {
		edgePosition = (int)(currentPage+1)*pageLengthInPx;
		parent.activePage = (int)currentPage+1;
	    }
	else
	    {
		edgePosition = (int)currentPage*pageLengthInPx;
		parent.activePage = (int)currentPage;
	    }
	
	Log.d(TAG, "Active Page :"+parent.activePage);
	this.smoothScrollTo((int)edgePosition,0);
	parent.activeArrayOfArticles = parent.collectionOfArticlesArrays.get(parent.activePage);
	parent.activeList = parent.collectionOfListViews.get(parent.activePage);
	parent.titleScroller.mainContentScrolled = true;
	parent.titleScroller.setSelectedTitleAtIndex(parent.activePage);
    }

    //scrolls directly to the page passed as argument.
    public void scrollToPage(int page){
	Log.d(TAG,"SCROLL TO PAGE:"+page);
	float currentPosition = this.getScrollX();
		
	float pagesCount = parent.contentContainer.getChildCount();
	float pageLengthInPx = parent.contentContainer.getMeasuredWidth()/pagesCount;
	float currentPage = currentPosition/pageLengthInPx; //the rounded value which defines current page
	int diff = (page - (int)currentPage);
	float deltaEdgePosition = diff * pageLengthInPx;
	float edgePosition = currentPosition + deltaEdgePosition;
	parent.activePage = page; //assign the new active page
	this.smoothScrollTo((int)edgePosition,0);

    	parent.activeArrayOfArticles = parent.collectionOfArticlesArrays.get(parent.activePage);
	parent.activeList = parent.collectionOfListViews.get(parent.activePage);

    }
}