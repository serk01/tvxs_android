package org.cwf.tvxs;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class TvxsDatabaseHelper extends SQLiteOpenHelper {
	private static final String TAG = "TvxsDatabaseHelper";
	private static final String DATABASE_NAME = "tvxsdb";

	private static final int DATABASE_VERSION = 1;

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table favourites (_id integer primary key autoincrement," +
			"title text not null," +
			"teaser text not null," +
			"fullarticle text, " +
			"articleurl text not null," +
			"imageurl text not null," +
			"pubdate text," +
			"image blob);";
	
	
	
	private static final String DATABASE_DELETE = "drop table if exists favourites";
	
	public TvxsDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Method is called during creation of the database
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);

		Set<String> set = FeedURLS.CATEGORIES_MAP.keySet();
		Iterator<String> itr  = set.iterator();
		while (itr.hasNext())
			createTableCategory(database, itr.next());
	}

	public void resetTable(SQLiteDatabase database)
	{
		database.execSQL(DATABASE_DELETE);
		database.execSQL(DATABASE_CREATE);
	}
	
	public void createTableCategory(SQLiteDatabase database, String category)
	{
		String categoryClean = Utils.cleanCategoryString(category);		
		String DATABASE_CREATE_CATEGORY = "create table " + categoryClean  + "(_id integer primary key autoincrement," +
				"title text not null," +
				"teaser text not null," +
				"fullarticle text, " +
				"articleurl text not null," +
				"imageurl text not null," +
				"pubdate text," +
				"image blob);";
		
		database.execSQL(DATABASE_CREATE_CATEGORY);
		
		
	}
	
	// Method is called during an upgrade of the database, e.g. if you increase
	// the database version
	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(TvxsDatabaseHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		database.execSQL("DROP TABLE IF EXISTS favourites");
		onCreate(database);
	}
}