package org.cwf.tvxs;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Environment;

public class TvxsDbAdapter {

	private static final String TAG = "TvxsDbAdapter";
	// Database fields

	private static final String DATABASE_TABLE = "favourites";
	private Context context;
	private SQLiteDatabase database;
	private TvxsDatabaseHelper dbHelper;
	private Cursor cursor = null;
	private ArrayList<String> cleanedTables = new ArrayList<String>();
	public TvxsDbAdapter(Context context) {
		this.context = context;
	}

	public TvxsDbAdapter open() throws SQLException {
		dbHelper = new TvxsDatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		dbHelper.close();
	}

	public boolean articleExists(String title)
	{
		Cursor c = fetchFavouriteByTitle(title);
		return c.getCount()!=0;
	}

	public long saveArticleOffline(String table, TvxsArticle article) throws IOException
	{
		String title = article.getTitle();
		String articleUrl = article.getArticleUrl();
		String imageUrl = article.getImageUrl();
		String pubDate = article.getPubDate();
		String teaser = article.getTeaser();
		String fullArticle = "testing";
		
		//clean once
		if (cleanedTables.isEmpty())
			cleanedTables.clear();

		//check if table has been cleaned. We don't want to be cleaning it 
		//before each article!
		if (!cleanedTables.contains(table))
		{
			final String DATABASE_DELETE = "drop table if exists " + table;
			Log.d(TAG, "DELETE COMMAND :"+DATABASE_DELETE);
			database.execSQL(DATABASE_DELETE);
			cleanedTables.add(table);
			
			//create new one
			dbHelper.createTableCategory(database, table);
		}

		HttpEntity imageEntity = null;
		byte[] imageBlob = null;
		try {
			imageEntity = getFullImage(imageUrl);
			imageBlob = EntityUtils.toByteArray(imageEntity);
		} catch (Exception e)
		{
//			e.printStackTrace();
			Log.d(TAG,"Failed to get image from " + imageUrl);
		}
		
		ContentValues initialValues = createContentValues(title, teaser, 
				pubDate, imageUrl, articleUrl, fullArticle, imageBlob);

		return database.insert(table, null, initialValues);

	}

	public Cursor getOfflineArticlesForCategory(String table)
	{
		return database.query(table, new String[] { Constants.KEY_ROWID, Constants.KEY_TITLE,
				Constants.KEY_TEASER, Constants.KEY_FULL_ARTICLE,
				Constants.KEY_PUBDATE, Constants.KEY_IMAGE,
				Constants.KEY_ARITCLE_URL, Constants.KEY_IMGURL}, null, null, null,
				null, null);
	}

	/**
	 * Create a new favourite article If successfully created return the new
	 * rowId for that article, otherwise return a -1 to indicate failure.
	 * @throws IOException 
	 */
	public long createFavourite(String title, String teaser, 
			String pubdate, String imageurl, String articleurl, String fullarticle) throws IOException {
		boolean articleExists = articleExists(title);
		if (!articleExists)
		{
			HttpEntity imageEntity = getFullImage(imageurl);
			byte[] imageBlob = EntityUtils.toByteArray(imageEntity);

			ContentValues initialValues = createContentValues(title, teaser, 
					pubdate, imageurl, articleurl, fullarticle, imageBlob);
			
			
			if (initialValues!=null)
			{
				return database.insert(DATABASE_TABLE, null, initialValues);
			} else
				return -1;
		} else 
			return -1;
	}

	/**
	 * Update a favourite article
	 * @throws IOException 
	 */
	public boolean updateFavourite(long rowId, String title, String teaser, 
			String pubdate, String imageurl, String articleurl, String fullarticle) throws IOException
			{
		ContentValues updateValues = createContentValues(title, teaser, 
				pubdate, imageurl, articleurl, fullarticle, null);
		return database.update(DATABASE_TABLE, updateValues, Constants.KEY_ROWID + "="
				+ rowId, null) > 0;
			}

	/**
	 * Deletes favourite article
	 */
	public boolean deleteFavourite(long rowId) {
		return database.delete(DATABASE_TABLE, Constants.KEY_ROWID + "=" + rowId, null) > 0;
	}

	public boolean deleteFavourite(String title) {
		String sqlSt = Constants.KEY_TITLE + "= ?";		
		return database.delete(DATABASE_TABLE, sqlSt, new String[] {title}) > 0;
	}

	/**
	 * Return a Cursor over the list of all favourite articles in the database
	 * 
	 * @return Cursor over all notes
	 */
	public Cursor fetchAllFavourites() {
		return database.query(DATABASE_TABLE, new String[] { Constants.KEY_ROWID, Constants.KEY_TITLE,
				Constants.KEY_TEASER, Constants.KEY_FULL_ARTICLE,
				Constants.KEY_PUBDATE, Constants.KEY_IMAGE,
				Constants.KEY_ARITCLE_URL, Constants.KEY_IMGURL}, null, null, null,
				null, null);
	}

	/**
	 * Return a Cursor positioned at the defined favourite article
	 */

	public Cursor fetchFavourite(long rowId) throws SQLException {
		Cursor mCursor = database.query(true, DATABASE_TABLE, new String[] {
				Constants.KEY_ROWID, Constants.KEY_TITLE, 
				Constants.KEY_TEASER, Constants.KEY_FULL_ARTICLE, Constants.KEY_PUBDATE, 
				Constants.KEY_IMAGE, Constants.KEY_IMGURL, Constants.KEY_ARITCLE_URL},
				Constants.KEY_ROWID + "=" + rowId, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public Cursor fetchFavouriteByTitle(String title) throws SQLException {
		Log.d(TAG,"check if title exists " + title);
		String sqlSt = "select 1 from " + DATABASE_TABLE + " where " + Constants.KEY_TITLE + "= ?";		
		Cursor mCursor = database.rawQuery(sqlSt, new String[] {title});
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	private ContentValues createContentValues(String title, 
			String teaser, 
			String pubdate, 
			String imageurl, 
			String articleurl, 
			String fullarticle,
			byte[] image) throws IOException 
			{

		ContentValues values = new ContentValues();
		
		
		values.put(Constants.KEY_TITLE,title);
		values.put(Constants.KEY_TEASER, teaser);
		values.put(Constants.KEY_FULL_ARTICLE, fullarticle);
		values.put(Constants.KEY_PUBDATE, pubdate);
		values.put(Constants.KEY_ARITCLE_URL, articleurl);
		values.put(Constants.KEY_IMGURL, imageurl);
		values.put(Constants.KEY_IMAGE, image);
		
		return values;
					}
	
	private HttpEntity getFullImage(String imageurl)
	{
		HttpEntity entity = null;
		DefaultHttpClient mHttpClient = new DefaultHttpClient();
		URL url = null;
		try {
			url = new URL(imageurl);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		HttpGet mHttpGet = null;
//			mHttpGet = new HttpGet(URLEncoder.encode(url.toString(),"UTF-8"));
		mHttpGet = new HttpGet(imageurl);

		HttpResponse mHttpResponse = null;
		try {
			mHttpResponse = mHttpClient.execute(mHttpGet);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//NULL POINTER EXCEPTION??
		try {
			if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				entity = mHttpResponse.getEntity();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return entity;
	}



}