/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "Handles rss feed parsing"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
 */

package org.cwf.tvxs;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.SAXException;
import android.content.Context;
import org.cwf.tvxs.TvxsArticle;

public class TvxsRssHandler extends DefaultHandler {
	private boolean inItem = false;
	private boolean inTitle = false;
	private boolean inTeaser = false;
	private boolean inArticleUrl = false;
	private boolean inPubDate = false;
	private boolean inImageUrl = false;
	private int articlesCounter = 0;

	public ArrayList<TvxsArticle> articlesList = new ArrayList<TvxsArticle>();
	private String TAG = "TvxsRssHandler";

	//temporary conainters of an article's fields
	private String tmpTitle, tmpTeaser, tmpImageUrl, tmpArticleUrl, tmpPubDate, tmpCategory;

	public TvxsRssHandler(String categoryToParse) {
		this.tmpCategory = categoryToParse;
	}
	
	public void startElement(String uri, String name, String qName, Attributes atts){

		if (name.trim().equals("item"))
			inItem = true;
		else if (name.trim().equals("title")){
			inTitle = true;
			tmpTitle = "";
		}
		else if (name.trim().equals("link")){
			inArticleUrl = true;
			tmpArticleUrl = "";
		}
		else if (name.trim().equals("teaser")){
			inTeaser = true;
			tmpTeaser = "";
		}
		else if (name.trim().equals("pubDate")){
			inPubDate = true;
			tmpPubDate = "";
		}
		else if (name.trim().equals("image")){
			inImageUrl = true;
			tmpImageUrl = "";
		}
	}


	public void endElement(String uri, String name, String qName)
			throws SAXException {
		Boolean itemFinished = false;

		if (name.trim().equals("item")){
			inItem = false;
			itemFinished = true;
		}
		else if (name.trim().equals("title"))
			inTitle = false;
		else if (name.trim().equals("link"))
			inArticleUrl = false;
		else if (name.trim().equals("teaser")){
			inTeaser = false;
		}
		else if (name.trim().equals("pubDate"))
			inPubDate = false;
		else if (name.trim().equals("image"))
			inImageUrl = false;


		//if we finished parsing an item create a new article object out of it
		if (itemFinished){
			//final manipulations here before creating my TvxsArticleObject

			//try removing the last part of the rss pubDate object
			tmpPubDate = tmpPubDate.split("\\+")[0].toString();


			if (tmpTitle != null){
				TvxsArticle parsedArticle = new TvxsArticle(tmpTitle,
						tmpTeaser,
						tmpImageUrl,
						tmpArticleUrl,
						tmpPubDate,
						tmpCategory);

				articlesList.add(parsedArticle);
				articlesCounter = articlesCounter + 1;

			}		
		}
	}

	public ArrayList getParsedArticlesListUpToIndex(int index){
		int size = articlesList.size();
		if (index > size || index == 0)
			index = size;
		ArrayList returnItems = new ArrayList();
		for (int i = 0; i < index; i++){
			returnItems.add(articlesList.get(i));
		}

		return returnItems;
	}

	public void cleanArticlesList(){
		articlesList.clear();
	}

	public void characters(char ch[], int start, int length) {

		String chars = (new String(ch).substring(start, start + length));

		try {
			if (inTitle){
				tmpTitle = tmpTitle + chars;
			}

			else if (inTeaser){
				tmpTeaser = tmpTeaser + chars;
			}	    
			else if (inImageUrl)
				tmpImageUrl = tmpImageUrl + chars;
			else if (inArticleUrl)
				tmpArticleUrl = tmpArticleUrl + chars;
			else if (inPubDate)
				tmpPubDate = tmpPubDate + chars;

		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
	}
}

