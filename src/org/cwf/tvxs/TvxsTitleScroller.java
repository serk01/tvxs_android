/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "The class that handles the titles subview.Scrolling horizontally through the titles and tapping on them is handled here"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
*/

package org.cwf.tvxs;

import android.content.Context;
import android.graphics.Color;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.view.View.OnClickListener;


public class TvxsTitleScroller extends HorizontalScrollView implements OnClickListener {

    View prevView;
    View actView;
    private String mActiveString;
    public Boolean mainContentScrolled = false;
    private Boolean wasTitleTapped = false;
    
    int indexOfActive = 0;
    int count = 0;
    int offset = 0;
    int newOffset = 0;
    int screenWidth = 0;
    
    private AdapterView mAdapter = null;
    private LinearLayout internalWrapper;
    
    final static String TAG = "TvxsTitleScroller";
    TvxsContentScroller contentScroller;
    Tvxs parent;
   
    TvxsTitleScroller(Context context, TvxsContentScroller scr, int sW){
	super(context);
	contentScroller = scr;
	screenWidth = sW;
	
	RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,Constants.TITLE_SCROLLER_HEIGHT);
	//	this.setPadding(5,5,5,5);
	this.setId(Constants.TITLE_SCROLLER);
	params.addRule(RelativeLayout.BELOW, R.id.logo);
	this.setLayoutParams(params);
	
	internalWrapper = new LinearLayout(context);
	LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, Constants.TITLE_SCROLLER_HEIGHT);
	internalWrapper.setPadding(0, 3, 0, 2);
	internalWrapper.setLayoutParams(linearParams);
    
	internalWrapper.setOrientation(LinearLayout.HORIZONTAL);
	internalWrapper.setBackgroundColor(Color.BLACK);
	addView(internalWrapper);

	this.setHorizontalScrollBarEnabled(false);
    }
    
    public void addTitleToHorizontalScroller(String title, Context c){

    	Button btnTitle = new Button(c);
    	btnTitle.setPadding(10, 0, 10, 0);
    	RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(120, Constants.TITLE_SCROLLER_HEIGHT);
        
    	btnTitle.setId(count);
    	btnTitle.setTextSize(15);

    	btnTitle.setLayoutParams(params);
    	btnTitle.setText(title);
    	btnTitle.setOnClickListener(this);
    	internalWrapper.addView(btnTitle);
    	count+=1;
    }
    
    public void removeAllTitles(){
    	internalWrapper.removeAllViews();
    	count = 0;
    }

    public boolean onFling(MotionEvent  e1, MotionEvent  e2, float velocityX, float velocityY){
	int numberOfTitles = this.getChildCount();
	float titleLengthInPx = this.getMeasuredWidth()/numberOfTitles;
	newOffset = this.computeHorizontalScrollOffset();
		
	Log.d(TAG,"ACTIVE VIEW INDEX:"+indexOfActive);//this.getSelectedView());
    	return true;
    }

    public void setSelectedTitleAtIndex(int index){
	Log.d(TAG,"SET TITLE SELECTION:"+index);
	indexOfActive = index;
	int scrollX = 0;
	int btnWidth = 0;
		
	for (int i = 0; i < count; i++){
	    Button btn = (Button)internalWrapper.getChildAt(i);
	    if (i == index){
		btn.setBackgroundResource(R.color.green);
		//				btn.setTextColor(getResources().getColor(R.color.green))
		btnWidth = btn.getWidth();
		scrollX = btn.getLeft();
	    }
	    else
		btn.setBackgroundResource(R.color.light_grey);
	    //				btn.setTextColor(getResources().getColor(R.color.dark_grey));
	}
	if (wasTitleTapped){
	    Log.d(TAG,"INDEX TO GOTO:"+index);
	    contentScroller.scrollToPage(index);
	    wasTitleTapped = false;
	}
		
	this.scrollTo(scrollX-screenWidth/2 + btnWidth/2, 0);
	//		contentScroller.scrollToAppropriatePage();
	//		mActiveString = (String)this.getItemAtPosition(index);
	//		this.setSelection(index,true);
    }
    //
    public void setSelectedTitleForView(View v,int index){
	indexOfActive = index;
	setSelectedTitleAtIndex(index);
	//		colorizeView(v);
		 
    }	

    public void onClick(View v) {
	// TODO Auto-generated method stub
	Log.d(TAG,"PRESSED :"+v.getId());
	wasTitleTapped = true;
	setSelectedTitleAtIndex(v.getId());
		
    }
    
}