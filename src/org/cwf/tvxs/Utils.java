/***
    __author__ ="Serko Katsikian"
    __date__ = "2011"
    __description__ = "Some basic utilities used in the app"
    __copyright__ = "Copyright (C) 2011 Serko Katsikian"
    __license__ = "GPLv3"

    Tvxs-Android: A news reader for www.tvxs.gr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
 */

package org.cwf.tvxs;


import java.io.InputStream;
import java.io.OutputStream;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.content.Context;

public class Utils {
	public static void CopyStream(InputStream is, OutputStream os)
	{
		final int buffer_size=1024;
		try
		{
			byte[] bytes=new byte[buffer_size];
			for(;;)
			{
				int count=is.read(bytes, 0, buffer_size);
				if(count==-1)
					break;
				os.write(bytes, 0, count);
			}
		}
		catch(Exception ex){}
	}

	public static Boolean isWifiAvailable(Context c){
		final ConnectivityManager connMangr = (ConnectivityManager)c.getSystemService(Context.CONNECTIVITY_SERVICE);
		final android.net.NetworkInfo wifi = connMangr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo ni = connMangr.getActiveNetworkInfo();
		if (ni == null || !ni.isConnected())
			return false;
		return true;
	}
	
	public static String cleanCategoryString(String category)
	{
		String clean = category.replace(" ", "");
		clean = clean.replace("-","");
		return clean;
	}


}